import org.junit.jupiter.api.Test
import java.io.File

class TestDay1 {

    @Test
    fun testFirstLine() {
        assert(extractCalibrationValue("two1nine") == 29)
    }

    @Test
    fun testWithPrependedLetters() {
        assert(extractCalibrationValue("abcone2threexyz") == 13)
    }

    @Test
    fun testWithOneDigitTurningIntoOther() {
        assert(extractCalibrationValue("xtwone3four") == 24)
    }

    @Test
    fun testWholeSampleInput() {
        val lines = File("src/main/resources/sample-input-day1-pt2.txt").readLines()

        assert(sumCalibrationValues(lines) == 281)
    }
}