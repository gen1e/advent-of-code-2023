import java.io.File

fun main() {
    val lines = File("src/main/resources/input-day1.txt").readLines()

    println(sumCalibrationValues(lines))
}

fun sumCalibrationValues(lines: List<String>): Int {
    var result = 0

    for (line in lines) {
        result += extractCalibrationValue(line)
    }

    return result
}

val digitMap = mapOf(
    "zero" to '0',
    "one" to '1',
    "two" to '2',
    "three" to '3',
    "four" to '4',
    "five" to '5',
    "six" to '6',
    "seven" to '7',
    "eight" to '8',
    "nine" to '9'
)

/**
 * If the string has a spelled out digit it will return that digit
 * If it doesn't contain any then it wil return null
 */
fun containsSpelledOutDigit(str: String): Char? {
    for (digit in digitMap) {
        if (str.contains(digit.key)) {
            return digit.value
        }
    }
    return null
}

/**
 * Takes a line of chars and extracts the calibration value out of it
 */
fun extractCalibrationValue(line: String): Int {
    // Works by working inwards with 2 pointers on either end of the line to find the digits

    var i = 0
    var j = line.length-1
    var firstVal: Char? = null
    var secondVal: Char? = null

    while (i <= j && firstVal == null || secondVal == null) {
        if (firstVal == null) {
            if (line[i].isDigit()) {
                firstVal = line[i]
            } else {
                containsSpelledOutDigit(line.substring(0, i+1))?.let { firstVal = it }
            }

        }
        if (secondVal == null) {
            if (line[j].isDigit()) {
                secondVal = line[j]
            } else {
                containsSpelledOutDigit(line.substring(j, line.length))?.let { secondVal = it }
            }

        }

        if (firstVal == null) {
            i++
        }
        if (secondVal == null) {
            j--
        }
    }

    if (firstVal != null) {
        return "$firstVal$secondVal".toInt()
    }

    return -1
}