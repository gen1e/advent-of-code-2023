# Advent of Code 2023

This is the repo of my answers for [Advent of Code 2023](https://adventofcode.com/2023), obviously there are spoilers for potential answers in here, proceed at your own risk.
I'm doing it all in Kotlin and generally trying my best to optimiize for time over space, unless the problem requires otherwise.
